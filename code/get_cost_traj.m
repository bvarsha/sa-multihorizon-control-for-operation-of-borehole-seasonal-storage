function [cost,cost_cum] = get_cost_traj(U,X,pred,params_const,N_j,dt_j)
alpha_1 = params_const.a_hp;
alpha_2 = params_const.a_ch;
beta_1 = params_const.b_hp-params_const.a_hp*params_const.deltaT_hp_BT;
beta_2 = params_const.b_ch-params_const.a_ch*params_const.deltaT_ch_BT;

if length(dt_j)>1 || dt_j~=1
    if isempty(N_j)
        N_j=length(X)-1;
    end
    pred_resampled=nan(params_const.np,sum(N_j));
    i_sol=1;
    i_t=1;
    dt_i=nan(1,sum(N_j));
    for j=1:length(dt_j)
        for i=1:N_j(j)
            dt_i(i_sol)=dt_j(j);
            i_t_pred=mod(i_t,8760);
            i_t_pred_end=mod(i_t+dt_j(j)-1,8760);
            if i_t_pred<=i_t_pred_end
                pred_resampled(:,i_sol)=mean(pred(:,i_t_pred:i_t_pred_end),2);
            else
                pred_resampled(:,i_sol)=mean([pred(:,i_t_pred:end),pred(:,1:i_t_pred_end)],2);
            end
            i_t=i_t+dt_j(j);
            i_sol=i_sol+1;
        end
    end
else
    pred_resampled=pred;
    dt_i=1;
end

lambda = (pred_resampled(3,:)*params_const.nu_CO2+params_const.nu_el).*dt_i;
beta_3 = 2*(params_const.T_d_hs-pred_resampled(2,:)+params_const.deltaT_hp_a)/(params_const.T_d_hs+273.15);
beta_4 = 2*(pred_resampled(2,:)+params_const.deltaT_ch_a-params_const.T_d_cs)/(params_const.T_d_cs+273.15);       

cost=0;
cost_cum=nan(1,length(U));
for i=1:length(U)
    cost = cost + lambda(i)*[...
        alpha_1*X(i)+beta_1,alpha_2*X(i)+beta_2,...
        beta_3(i),beta_4(i),0,0,0]...
        *U(:,i);
    cost_cum(i) = cost;
end