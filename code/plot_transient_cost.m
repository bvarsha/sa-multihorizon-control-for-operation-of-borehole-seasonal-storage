clear all
%% load data
data_CL=cell(5,1);
data_SS=cell(length(data_CL),1);
labels={'MHMPC+','MHMPC+ (TC)','RBC','24h MPC','High-level MPC'}';
data_CL{1}=combine_data({...
    load("../simulation_output/231223_1425_CL_MHMPC_plus.mat"),...
    load("../simulation_output/231230_1444_CL_MHMPC_reshuffle_sc50_part2.mat")});
data_SS{1}=load("../simulation_output/231231_1554_SS_MHMPC_plus.mat");
data_CL{2}=load("../simulation_output/231226_1551_CL_MHMPC_plus_terminalCost_modified.mat");
data_SS{2}=load("../simulation_output/231226_1515_SS_MHMPC_plus_terminalCost.mat");
data_CL{3}=load("../simulation_output/231222_0957_CL_RBC_7_30y.mat");
data_SS{3}=load("../simulation_output/231214_1317_SS_rulebased_7_1h.mat");
data_CL{4}=combine_data({...
    load("../simulation_output/231222_1514_CL_MPC_24h.mat"),...
    load("../simulation_output/231230_1854_CL_MPC_24h_part2.mat")});
data_SS{4}=load("../simulation_output/231203_1542_SS_MPC_24h.mat");
data_CL{5}=combine_data({...
    load("../simulation_output/231222_1335_CL_MPC_highlevel.mat"),...
    load("../simulation_output/231231_0306_CL_MPC_highlevel_part2.mat")});
data_SS{5}=load("../simulation_output/231219_2006_SS_MPC_highlevel_try.mat");

%% processing

cost_yearly=cell(length(data_CL),1);
temp_0_yearly=cell(length(data_CL),1);
cost_cum_yearly=cell(length(data_CL),1);
for i_c=1:length(data_CL)
    maxYears=length(data_CL{i_c}.out.cost_cum)/8760;
    cost_cum=[0,data_CL{i_c}.out.cost_cum];
    cost_yearly{i_c}=diff(cost_cum(8760*(0:maxYears)+1));
    temp_0_yearly{i_c}=data_CL{i_c}.out.X(8760*(0:maxYears)+1);
    cost_cum_yearly{i_c}=cumsum(cost_yearly{i_c});
end

cost_yearly_plot=cell2mat_plot(cost_yearly);
temp_0_yearly_plot=cell2mat_plot(temp_0_yearly);
cost_cum_yearly_plot=cell2mat_plot(cost_cum_yearly);

%% evaluate cost of SS
cost_yearly_plot_SS=nan(1,length(data_SS));
temp_0_SS=nan(1,length(data_SS));
for i_c=1:length(data_SS)
    data=data_SS{i_c};
    if ~isfield(data,'prediction')
        data.prediction=generate_prediction;
    end
    terminalCost=4150;
    [cost,cost_cum] = get_cost_traj(data.out.U_SS,data.out.X_SS,data.prediction,data.params_constant,[],data.params_sim.dt);
    cost_yearly_uncorrected=cost*8760/data.params_sim.Tf;
    cost_yearly=cost_yearly_uncorrected+terminalCost*(data.out.X_SS(1)-data.out.X_SS(end));

    data.out.cost=cost;
    data.out.cost_cum=cost_cum;
    data_SS{i_c}=data;
    cost_yearly_plot_SS(i_c)=cost_yearly;
    temp_0_SS(i_c)=data.out.X_SS(1);
end

%% plot yearly cost

fig = figure();
fig.Position(3)=1.05*fig.Position(3);
fig.Position(4)=500;
set(fig, 'DefaultTextInterpreter', 'latex')
set(fig, 'DefaultLegendInterpreter', 'latex')
set(fig, 'DefaultAxesTickLabelInterpreter', 'latex')
set(fig, 'DefaultLineLineWidth', 1);
sz_font=16;

sp11=subplot(2,2,1);
ax11=gca;
plot(cost_yearly_plot'/1000)
xlabel('$t \ [y]$')
ylabel('$J \ [kCHF]$')

legend(labels,'Location','northeast')

sp12=subplot(2,2,2);
ax12=gca;
hold on
box on
x=repmat(linspace(0,4,8)',1,5);
x([3,4,7,8],2)=nan;
x([1,2,5,6],3)=nan;
for i_c=1:length(cost_yearly_plot_SS)
    plot(x(:,i_c),repmat(cost_yearly_plot_SS(i_c),1,8)/1000);
end
set(gca,'XTick',[], 'YTick', [])
xlabel('SS')

linkaxes([ax11,ax12],'y');

sp21=subplot(2,2,3);
ax21=gca;
plot(temp_0_yearly_plot')
xlabel('$t \ [y]$')
ylabel('$T_{BT,0} \ [^\circ C]$')
linkaxes([ax11,ax21],'x');
xlim([1,8])

sp22=subplot(2,2,4);
ax22=gca;
hold on
box on
x=repmat(linspace(0,4,8)',1,5);
x([3,4,7,8],2)=nan;
x([1,2,5,6],3)=nan;
for i_c=1:length(temp_0_SS)
    plot(x(:,i_c),repmat(temp_0_SS(i_c),1,8));
end
set(gca,'XTick',[], 'YTick', [])
linkaxes([ax21,ax22],'y');
xlabel('SS')

shift=0.22;
ex=0.06;

sp11.Position=sp11.Position+[0,0,shift+ex,0];
sp12.Position=sp12.Position+[shift,0,-shift,0];
sp21.Position=sp21.Position+[0,0,shift+ex,0];
sp22.Position=sp22.Position+[shift,0,-shift,0];

fontsize(fig, sz_font, "points")

%% plot cumulative yearly cost

fig2 = figure();
fig2.Position(3)=1.05*fig2.Position(3);
fig2.Position(4)=300;
set(fig2, 'DefaultTextInterpreter', 'latex')
set(fig2, 'DefaultLegendInterpreter', 'latex')
set(fig2, 'DefaultAxesTickLabelInterpreter', 'latex')
set(fig2, 'DefaultLineLineWidth', 1);
sz_font=16;

cost_cum_SS=min(cost_yearly_plot_SS)*(1:size(cost_cum_yearly_plot,2));
cost_cum_yearly_diff_plot=cost_cum_yearly_plot-repmat(cost_cum_SS,5,1);
plot(cost_cum_yearly_diff_plot'/1000)
xlim([1,8])
ylim([0,120])

xlabel('$t \ [y]$')
ylabel('$ \sum \Delta J \ [kCHF]$')
legend(labels,'Location','southeast')

fontsize(fig2, sz_font, "points")


function out=cell2mat_plot(cellarray)
maxYears=0;
for i_c=1:length(cellarray)
    maxYears=max(length(cellarray{i_c}),maxYears);
end
out=nan(length(cellarray),maxYears);
for i_c=1:length(cellarray)
    out(i_c,1:length(cellarray{i_c}))=cellarray{i_c};
end
end
