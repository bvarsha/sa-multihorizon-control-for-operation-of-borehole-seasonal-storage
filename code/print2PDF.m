function print2PDF(filename, fig)
set(fig,'Units','centimeters');
screenposition = get(fig,'Position');
set(fig,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)]);
print('-dpdf', '-vector', '-r300', ['../figures/',filename]) 
savefig(fig,['../figures/',filename]);
end