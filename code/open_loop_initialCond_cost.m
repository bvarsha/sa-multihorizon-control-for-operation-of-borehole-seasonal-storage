yalmip('clear')
clear all

% get all paramters which are constant in time
params_constant=generate_params_constant;
prediction=generate_prediction;

params_MPC=[...
    generate_params_MPC(24 * 365,24*[60,5],[6,1]),...
    generate_params_MPC(24 * 365,24*[45,5],[8,1]),...
    generate_params_MPC(24 * 365,24*[40,5],[9,1]),...
    generate_params_MPC(24 * 365,24*[30,5],[12,1]),...
    generate_params_MPC(24 * 365,24*[25,15],[14,1]),...
    generate_params_MPC(24 * 365,24*[20,5],[18,1]),...
    generate_params_MPC(24 * 365,24*[15,5],[24,1]),...
    generate_params_MPC(24 * 365,24*[10,5],[36,1]),...
    generate_params_MPC(24 * 365,24*[7,1],[52,1]),...
    generate_params_MPC(24 * 365,24*5,73),...
    generate_params_MPC(24 * 365,24*[3,2],[121,1]),...
    generate_params_MPC(24 * 365,24*[2,1],[182,1]),...
    ];

legendentries=[...
    "$\Delta t = 60$d",...
    "$\Delta t = 45$d",...
    "$\Delta t = 40$d",...
    "$\Delta t = 30$d",...
    "$\Delta t = 25$d",...
    "$\Delta t = 20$d",...
    "$\Delta t = 15$d",...
    "$\Delta t = 10$d",...
    "$\Delta t = 7$d",...
    "$\Delta t = 5$d",...
    "$\Delta t = 3$d",...
    "$\Delta t = 2$d",
    ];

% params_MPC=params_MPC(1:3);
% legendentries=legendentries(1:3);

T_BTES_0=8:1:55;

N_T0=length(T_BTES_0);
N_controller=length(params_MPC);

cost=nan(N_controller,N_T0);
figure

for j_controller=1:N_controller
    MPC_obj = MPC(params_MPC(j_controller),params_constant);
    for j_T0=1:N_T0
        [U, X, ctrl_info,log_obj]=MPC_obj.eval(T_BTES_0(j_T0),prediction);
        if ctrl_info.objective==0
            obj=nan;
        else
            obj=ctrl_info.objective;
        end
        cost(j_controller,j_T0)=obj;
        plot(meshgrid(T_BTES_0,1:N_controller)',cost(end:-1:1,:)'./1000);
        drawnow
    end
end

data=struct;
data.T_BTES_0=T_BTES_0;
data.params_MPC=params_MPC;
data.cost=cost;
data.legendentries=legendentries;

%% plot the minimum temperatures
fig = figure(); 
set(fig, 'DefaultTextInterpreter', 'latex')
set(fig, 'DefaultLegendInterpreter', 'latex')
set(fig, 'DefaultAxesTickLabelInterpreter', 'latex')
set(fig, 'DefaultLineLineWidth', 1);
fig.Position(3)=1.05*fig.Position(3);
fig.Position(4)=400;
hold on
box on
sz_font=16;
N_controller=length(data.params_MPC);

[min_cost,min_temp_idx]=min(data.cost,[],2);

plot(meshgrid(data.T_BTES_0,1:N_controller)',data.cost(end:-1:1,:)'./1000);
hold on
plot(data.T_BTES_0(min_temp_idx),min_cost./1000,'black','Marker','*');

legend([data.legendentries(end:-1:1),"minimum cost"],'Location','north','NumColumns',3)
xlabel('$T_{BT,0} \ [^\circ C]$')
ylabel('$J \ [kCHF]$')

xlim([min(data.T_BTES_0),max(data.T_BTES_0)]);

ylim_org=ylim;
ylim([ylim_org(1),ylim_org(2)+10]);


fontsize(fig, sz_font, "points")

%% save 

datename=char(datetime,'yyMMdd_HHmm');
filename=[datename,'_open_loop_initialCond_cost'];

save(['../simulation_output/',filename,'.mat'],'-struct','data');



