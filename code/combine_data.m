function data_out=combine_data(data)

% remove years of nan entries if present
for i=1:length(data)
    nanYears=floor(sum(isnan(data{i}.out.X))/8760);
    if nanYears>=1
        data{i}.out.X=data{i}.out.X(:,1:end-8760);
        data{i}.out.U=data{i}.out.U(:,1:end-8760);
        data{i}.out.t_solve=data{i}.out.t_solve(:,1:end-8760);
        data{i}.out.cost_cum=data{i}.out.cost_cum(:,1:end-8760);
    end
end

data_out=data{1}; % the first data element is the baseline

for i=2:length(data)
    data_out.out.X=[data_out.out.X,data{i}.out.X(:,2:end)];
    data_out.out.U=[data_out.out.U,data{i}.out.U];
    data_out.out.t_solve=[data_out.out.t_solve,data{i}.out.t_solve];
    data_out.out.cost_cum=[data_out.out.cost_cum,data_out.out.cost_cum(end)+data{i}.out.cost_cum];
end
end