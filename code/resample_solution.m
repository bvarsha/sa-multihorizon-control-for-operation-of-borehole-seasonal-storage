function [X_plot,U_plot,log_obj_plot]=resample_solution(X,U,log_obj,params_const,type,params_MPC_sim)
nu = params_const.nu;
nx = params_const.nx;
B_avg_daily=0;
switch type
    case 'MPC'
        N_j = params_MPC_sim.N_j;
        dt_j = params_MPC_sim.dt_j;
        N=params_MPC_sim.Tf;
    case 'CL'
        dt_j = params_MPC_sim.dt;
        N=size(U,2);
        N_j = N/dt_j;
        if dt_j==1
            B_avg_daily=1;
        end
end
X_plot=nan(nx,N+1);
U_plot=nan(nu,N+1);
log_obj_plot=struct;
if ~isempty(log_obj)
    names_field=fieldnames(log_obj);
else
    names_field={};
end
for i_field=1:length(names_field)
    h=height(log_obj.(names_field{i_field}));
    log_obj_plot.(names_field{i_field})=nan(h,N+1);
end

k_out=1;
k_sol=1;
for j = 1:length(N_j)
    dt=dt_j(j);
    for k = 1:N_j(j)
        X_plot(:,k_out:k_out+dt-1)=repmat(X(:,k_sol),1,dt);
        U_plot(:,k_out:k_out+dt-1)=repmat(U(:,k_sol),1,dt);
        
        for i_field=1:length(names_field)
            val=log_obj.(names_field{i_field})(:,k_sol);
            val=repmat(val,1,dt);
            log_obj_plot.(names_field{i_field})(:,k_out:k_out+dt-1)=val;
        end

        k_out=k_out+dt;
        k_sol=k_sol+1;
    end
end

X_plot(end)=X(end);

if B_avg_daily
    for i_day=1:ceil(N/24)
        i_start=1+(i_day-1)*24;
        U_plot(:,i_start:i_start+23)=repmat(mean(U_plot(:,i_start:i_start+23),2),1,24);
    end
end

end
