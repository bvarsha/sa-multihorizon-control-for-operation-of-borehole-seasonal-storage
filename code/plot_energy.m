function plot_energy(X,U,params_constant,params_offline,T_BTES_0,pred)
% NOT READY
[~,P_BT_loss_top,P_BT_loss_g] = getOpenLoopPrediction(T_BTES_0,U,params_constant,params_offline,pred);

% Add a nan entry at the end of the input and prediction sequence so it has 
% the same length as the state sequence
P_BT_loss_top=[P_BT_loss_top,nan(height(P_BT_loss_top),1)];
P_BT_loss_g=[P_BT_loss_g,nan(height(P_BT_loss_g),1)];
pred=[pred,nan(height(pred),1)];

figure
subplot(2,1,1)
hold on
plot(xRange,X')
plot(xRange,pred(2,:)')
plot(xRange,params_constant.T_g*ones(1,length(xRange)))
title('Temperatures')
legend({'T_BT','T_a','T_g'},'Interpreter','none')
ylabel('[°C]')
ax1=gca;

subplot(2,1,2)
hold on
plot(xRange,-U(2,:)')
plot(xRange,-U(7,:)')
plot(xRange,U(1,:)')
plot(xRange,U(5,:)')
plot(xRange,P_BT_loss_top')
plot(xRange,P_BT_loss_g')
title('BTES thermal energy flows')
legend({'P_th_ch_BT','P_sol,tr','P_th_hp_BT','P_th_BT_direct','P_BT_loss_top','P_BT_loss_g'},'Interpreter','none','Location','southeast')
ylabel('[kW]')
ax2=gca;

strech=0.15;
pos1=ax1.Position;
pos2=ax2.Position;
ax1.Position=pos1+[0 strech 0 -strech];
ax2.Position=pos2+[0 0 0 strech];

linkaxes([ax1,ax2],'x')

% E_BT_charge=sum(U(2,1:end-1))+sum(U(7,1:end-1));
% E_BT_discharge=sum(U(1,1:end-1))+sum(U(5,1:end-1))+sum(P_BT_loss_top(1:end-1))+sum(P_BT_loss_g(1:end-1));
end