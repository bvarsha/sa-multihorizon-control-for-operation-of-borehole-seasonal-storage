function pred_avg=average_prediction(pred,avg_interval)

N_pred=length(pred);

avg_interval=mod(avg_interval,N_pred); %if the prediction exceeds the array bounds, subtract 8760
avg_interval(avg_interval==0)=N_pred; % use 8760 as index instead of 0;

pred_avg=mean(pred(:,avg_interval),2);

%the demand needs to be averaged and then we only apply a cooling or
%heating demand

net_avg=pred_avg(4)-pred_avg(5);
pred_avg(4)=max(0,net_avg); % kW
pred_avg(5)=-min(0,net_avg); % kW
end