classdef MPC
    properties
        yalmip_optimizer
        yalmip_optimizer_check
        create_time
        log_obj
        params_MPC
        params_const
    end

    methods
        function obj = MPC(params_MPC,params_const,varargin)
            tic

            nu = params_const.nu;
            nx = params_const.nx;
            
            dt_j=params_MPC.dt_j;
            N_j=params_MPC.N_j;
            N=sum(N_j);
            
            obj.params_MPC=params_MPC;
            obj.params_const=params_const;

            U=sdpvar(nu,N,'full');
            X=sdpvar(nx,N+1,'full');
            X0 = sdpvar(nx,1,'full');
            Xend = sdpvar(nx,1,'full');

            C = sdpvar(1,N,'full');
            lambda = sdpvar(1,N,'full');
            beta_3 = sdpvar(1,N,'full');
            beta_4 = sdpvar(1,N,'full');
            
            % dynamics parameters
            kappa=60*60/obj.params_const.c_p_g_vol/obj.params_const.V;
            UA_BT_top=obj.params_const.U_i*obj.params_const.D^2/4*pi(); % look again at the value
            UA_BT_g=obj.params_const.k_g*obj.params_const.h*obj.params_const.D/2;
            A = 1 - dt_j*kappa*(UA_BT_top+UA_BT_g);
            B_T = [-1 1 0 0 -1 0 1]'*dt_j*kappa; %B transpose

            % generate the constant parameters
            alpha_1 = params_const.a_hp;
            alpha_2 = params_const.a_ch;
            beta_1 = params_const.b_hp-params_const.a_hp*params_const.deltaT_hp_BT;
            beta_2 = params_const.b_ch-params_const.a_ch*params_const.deltaT_ch_BT;

            %
            if strcmp(params_MPC.terminalConstraint,'lastYear')
                % find the point which is one year before the end
                dt_sum_end=0; %time to the end of horizon
                k_sol_end=0; % sample points to the end of horizon
                B_sol_found=0;
                j=length(N_j);
                k=1;
                while j>=1 && ~B_sol_found
                    while k<=N_j(j) && ~B_sol_found
                        dt_sum_end=dt_sum_end+dt_j(j);
                        k_sol_end=k_sol_end+1;
                        if dt_sum_end>=365*24
                            k_sol_lastYearConstraint=N-k_sol_end+1;
                            B_sol_found=1;
                        end
                        k=k+1;
                    end
                    k=1;
                    j=j-1;
                end
                if ~B_sol_found
                    error('The lastYear constraint/scaling could not be generated');
                end
            end

            constraints = [];
            objective = 0;
            cost_economic=0;
            if params_MPC.B_initial_cond
                X(1) = X0; 
            else
                % we can set an upper bound on the inital condition
                %constraints=[constraints,X(:,1)<=X0];
            end
            l=sdpvar(1,N,'full');
            k_t=1;
            k_sol=1;
            scaling=1;
            for j = 1:length(N_j)
                for k = 1:N_j(j)
                    if strcmp(params_MPC.terminalConstraint,'lastYear') &&...
                            k_sol>=k_sol_lastYearConstraint
                        scaling=params_MPC.lastYearScaling;
                    end
                    % objective
                    % l(k_sol)=scaling*lambda(k_sol)*[...
                    %     alpha_1*X(k_sol)+beta_1,alpha_2*X(k_sol)+beta_2,...
                    %     beta_3(k_sol),beta_4(k_sol),0,0,0]...
                    %     *U(:,k_sol);
                    
                    Q=zeros(params_const.nx+params_const.nu);
                    Q(1,2)=1/2*alpha_1;
                    Q(2,1)=1/2*alpha_1;
                    Q(1,3)=1/2*alpha_2;
                    Q(3,1)=1/2*alpha_2;

                    q=[0,beta_1,beta_2,beta_3(k_sol),beta_4(k_sol),0,0,0];
                    
                    l(k_sol)=lambda(k_sol)*(...
                        [X(k_sol);U(:,k_sol)]'*Q*[X(k_sol);U(:,k_sol)]+...
                        q*[X(k_sol);U(:,k_sol)]);

                    % l(k_sol)=lambda(k_sol)
                    objective = objective + scaling*l(k_sol);
                    cost_economic = cost_economic + l(k_sol);
                    
                    % dynamics
                    X(:,k_sol+1)=A(j)*X(k_sol)+B_T(:,j)'*U(:,k_sol)+C(k_sol);
                    
                    % update indices
                    k_t=k_t+dt_j(j);
                    k_sol=k_sol+1;
                end
            end

            % terminal cost
            switch params_MPC.terminalCost
                case 'linear'
                    k_end=params_MPC.l_end;
                    objective = objective + k_end*(X(:,end)-X(:,1));
            end
            
            %% Constraints

            % constant input inequality constraints
            constraints_const_ineq_InputMatrix=[-eye(7);eye(7)];
            constraints_const_ineq_InputLimit=[...
                zeros(7,1);...
                params_const.UA_heat*params_const.deltaT_hp_BT;...
                params_const.UA_cool*params_const.deltaT_ch_BT;...
                params_const.P_hp_a_max;...
                params_const.P_ch_a_max;...
                10^5;...
                10^5;...
                10^5 ...
                ];
            constraints_const_ineq_InputLimit_all=repmat(constraints_const_ineq_InputLimit,1,N);
            constraints=[constraints,constraints_const_ineq_InputMatrix*U<=constraints_const_ineq_InputLimit_all];

            % online input/state inequality constraints
            constraints_on_ineq_InputMatrix=[0 0 0 0 1 0 0];
            constraints_on_ineq_InputLimit_all=max(0,params_const.UA_heat*(X(:,1:end-1)-params_const.T_d_hs));
            constraints=[constraints,constraints_on_ineq_InputMatrix*U<=constraints_on_ineq_InputLimit_all];

            % offline input equality constraints
            constraints_off_eq_InputMatrix=[1 0 1 0 1 1 0; 0 1 0 1 0 0 0; 0 0 0 0 0 1 1];
            constraints_off_eq_InputLimit_all=sdpvar(3,N,'full');
            constraints=[constraints,constraints_off_eq_InputMatrix*U==constraints_off_eq_InputLimit_all];

            % constant state inequality constraints
            constraints_const_ineq_StateMatrix=[1;-1];
            constraints_const_ineq_StateLimit=[params_const.T_BT_max;-params_const.T_BT_min];
            constraints_const_ineq_StateLimit_all=repmat(constraints_const_ineq_StateLimit,1,N+1);
            constraints=[constraints,constraints_const_ineq_StateMatrix*X<=constraints_const_ineq_StateLimit_all];
            %also add it for X0, probably reduntant
            constraints=[constraints,constraints_const_ineq_StateMatrix*X0<=constraints_const_ineq_StateLimit]; 

            % add terminal constraint
            switch params_MPC.terminalConstraint
                case 'initialCond'
                    constraints=[constraints,X(:,end)==X(:,1)];
                case 'lastYear'
                    constraints=[constraints,X(:,end)==X(:,k_sol_lastYearConstraint)];
                case 'forced'
                    constraints=[constraints,X(:,end)==params_MPC.x_end];
                case 'none'
            end
            
            %%

            opts = sdpsettings('verbose',params_MPC.verbose,'solver','gurobi');
            opts.gurobi.NonConvex=2;
            opts.gurobi.MIPGap=params_MPC.MIPGap;
            %opts.gurobi.NumericFocus=3;
            obj.yalmip_optimizer = optimizer(constraints,objective,opts,...
                {...
                X0,...
                Xend,...
                C,...
                lambda,...
                beta_3,...
                beta_4,...
                constraints_off_eq_InputLimit_all,...
                },...
                {U,X,objective,cost_economic,l});

            % obj.yalmip_optimizer_check = optimizer(constraints,objective,opts,...
            %     {...
            %     X0,...
            %     Xend,...
            %     C,...
            %     lambda,...
            %     beta_3,...
            %     beta_4,...
            %     constraints_off_eq_InputLimit_all,...
            %     U,...
            %     },...
            %     {X,objective,l});

            obj.create_time=toc;
        end

        function [T_BTES_0,T_BTES_end,C,lambda,beta_3,beta_4,constraints_off_eq_InputLimit_all]...
                = precompute_opt_params(obj,T_BTES_0,pred,varargin)
            T_BTES_end=1;
            if length(varargin)>1 || ~isempty(varargin{1})
                T_BTES_end=varargin{1};
            end
            
            B_reshuffle=isfield(obj.params_MPC,'dt_day');

            if B_reshuffle
                dt_day=obj.params_MPC.dt_day;
                dt_h=obj.params_MPC.dt_h;
                pred_reshaped={...
                    reshape(pred(1,:),[24,365]);...
                    reshape(pred(2,:),[24,365]);...
                    reshape(pred(3,:),[24,365]);...
                    reshape(pred(4,:),[24,365]);...
                    reshape(pred(5,:),[24,365])};
            end

            np=obj.params_const.np;
            N_j=obj.params_MPC.N_j;
            dt_j=obj.params_MPC.dt_j;
            N=sum(N_j);
            
            pred_MPC=nan(np,N);
            % preprocess the prediction
            dt_k_sol=nan(1,N);
            k_t=1;
            k_sol=1;
            for j=1:length(dt_j)
                if ~B_reshuffle || (length(dt_h)==1 && isnan(dt_h)) || isnan(dt_h(j))
                    for k = 1:N_j(j)
                        avg_interval=k_t+(0:dt_j(j)-1);
                        pred_MPC(:,k_sol)=average_prediction(pred,avg_interval);
                        dt_k_sol(k_sol)=dt_j(j);
    
                        % update indices
                        k_t=k_t+dt_j(j);
                        k_sol=k_sol+1;
                    end
                else
                    for j_days = 1:N_j(j)*dt_h(j)/24
                        for j_h = 1:24/dt_h(j)
                            offset_days=(k_t-1)/24+(j_days-1)*dt_day(j);
                            avg_days=offset_days+(1:dt_day(j));
                            avg_h=(j_h-1)*dt_h(j)+(1:dt_h(j));
        
                            pred_MPC(:,k_sol)=average_prediction_reshaped(pred_reshaped,avg_days,avg_h);
                            dt_k_sol(k_sol)=dt_j(j);
        
                            % update indices
                            k_sol=k_sol+1;
                        end
                    end
                    k_t=k_t+N_j(j)*dt_h(j)*dt_day(j);
                end
            end
            
            % generate the parameters
            % dynamics parameters
            kappa=60*60/obj.params_const.c_p_g_vol/obj.params_const.V;
            UA_BT_top=obj.params_const.U_i*obj.params_const.D^2/4*pi(); % look again at the value
            UA_BT_g=obj.params_const.k_g*obj.params_const.h*obj.params_const.D/2;
            C = dt_k_sol*kappa.*(UA_BT_top*pred_MPC(2,:)+UA_BT_g*obj.params_const.T_g);
            
            % objective function parameters
            lambda = (pred_MPC(3,:)*obj.params_const.nu_CO2+obj.params_const.nu_el).*dt_k_sol;
            beta_3 = 2*(obj.params_const.T_d_hs-pred_MPC(2,:)+obj.params_const.deltaT_hp_a)/(obj.params_const.T_d_hs+273.15);
            beta_4 = 2*(pred_MPC(2,:)+obj.params_const.deltaT_ch_a-obj.params_const.T_d_cs)/(obj.params_const.T_d_cs+273.15);
            
            % constraints parameters
            constraints_off_eq_InputLimit_all=[...
                pred_MPC(4:5,:);...
                obj.params_const.eta_sol*obj.params_const.A_sol*pred_MPC(1,:)];
        end

        function [U, X, ctrl_info,log_obj] = eval(obj,T_BTES_0,pred,varargin)
            [T_BTES_0,T_BTES_end,C,lambda,beta_3,beta_4,constraints_off_eq_InputLimit_all]...
                = precompute_opt_params(obj,T_BTES_0,pred,varargin);
            i_try=1;
            feasible = false;
            while ~feasible && i_try<=5
                % solve the MPC problem for the given parameters
                tic;
                if i_try==1
                    T_BTES_0_used=T_BTES_0;
                else
                    T_BTES_0_used=round(T_BTES_0,5-i_try+1);
                end
                [optimizer_out,errorcode] = obj.yalmip_optimizer(...
                    {...
                    T_BTES_0_used,...
                    T_BTES_end,...
                    C,...
                    lambda,...
                    beta_3,...
                    beta_4,...
                    constraints_off_eq_InputLimit_all,...
                    });
                solvetime = toc;
                
                [U, X, objective, cost_economic, l] = optimizer_out{:};
    
                if (errorcode == 0)
                    feasible = true;
                    if i_try>1
                        fprintf('MPC was feasible after %d tries \n',i_try);
                    end
                end
                if i_try>=5 && ~feasible
                        msg=sprintf('MPC was infeasible after %d tries \n',i_try);
                        warning(msg);
                        break
                end
                log_obj=obj.log_obj;
                i_try=i_try+1;
            end

            ctrl_info = struct('ctrl_feas',feasible,'objective',objective, 'cost_economic',cost_economic,'solvetime',solvetime,'cost_stage',l);
        end

        function [X,ctrl_info] = checkTraj(obj, T_BTES_0, U, pred,varargin)
            [T_BTES_0,T_BTES_end,C,lambda,beta_3,beta_4,constraints_off_eq_InputLimit_all]...
                = precompute_opt_params(obj,T_BTES_0,pred,varargin);

            tic;
            [optimizer_out,errorcode] = obj.yalmip_optimizer_check(...
                {...
                T_BTES_0,...
                T_BTES_end,...
                C,...
                lambda,...
                beta_3,...
                beta_4,...
                constraints_off_eq_InputLimit_all,...
                U,...
                });

            [X, objective, cost_economic, l] = optimizer_out{:};
            feasible = true;
            if (errorcode ~= 0)
                feasible = false;
            end
            solvetime = toc;

            ctrl_info = struct('ctrl_feas',feasible,'objective',objective,'cost_economic',cost_economic,'solvetime',solvetime,'cost_stage',l);
        end
    end
    
end

function pred_avg=average_prediction_reshaped(pred_reshaped,avg_days,avg_h)
N_pred=length(pred_reshaped{1});

avg_days=mod(avg_days,N_pred); %if the prediction exceeds the array bounds, subtract 8760
avg_days(avg_days==0)=N_pred; % use 8760 as index instead of 0;

np=length(pred_reshaped);
pred_avg=cell(np,1);
for i=1:np
    pred_avg{i}=mean(pred_reshaped{i}(avg_h,avg_days),1:2);
end

pred_avg=cell2mat(pred_avg);

%the demand needs to be averaged and then we only apply a cooling or
%heating demand

net_avg=pred_avg(4)-pred_avg(5);
pred_avg(4)=max(0,net_avg); % kW
pred_avg(5)=-min(0,net_avg); % kW
end
