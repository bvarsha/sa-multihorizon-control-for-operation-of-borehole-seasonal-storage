%%% ---- Main Script to run the optimization problem ---- %%%
%%% ---- The script requires the installation of YALMIP and GUROBI

yalmip('clear')
clear all

testname=input('Please enter the test name (leave empty to not save): ',"s");

% params_controller=generate_params_MPC(24 * 365*2,24*5,73*2, ...
%     'terminalConstraint','lastYear', 20, ...
%     'MIPGap',5e-2);
% params_controller=generate_params_MPC(24 * 365*2,24*[5,10,15,30,5,30],[4,3,3,9,1,12], ...
%     'terminalConstraint','lastYear', 20, ...
%     'MIPGap',5e-2);
% params_controller=generate_params_MPC(24 * 365*2,[1,3,8,24,24*7,24*7*4,24,24*7*4],[6,6,6,5,7,11,1,13],...
%             'terminalConstraint','lastYear',20, ...
%             'MIPGap',5e-2);
% params_controller=generate_params_MPC(24 * 365*2,24*[1,3,7,28,1,28],[3,4,6,11,1,13],...
%             'terminalConstraint','lastYear',20, ...
%              'MIPGap',5e-2);
% params_controller=generate_params_MPC(24 * 365,24*3,365/3,'terminalConstraint','initialCond','verbose',2);
% params_controller=generate_params_MPC(24 * 365,24*[2,4,7,14,28],[4,3,5,4,4,9],'terminalConstraint','initialCond');
%params_controller=generate_params_MPC(24 * 365,[1,24,24*7,24*7*4],[24,7,5,365/7/4]);
% params_controller=generate_params_MPC(24 * 365 *2,[1,3,6,24*[1,2,7,28,1,28]],[3,3,2,3,2,3,12,1,13],...
%     'B_reshuffle',1,[nan,nan,nan,8,8,8,8,nan,nan], ...
%     'terminalConstraint','lastYear',50, ...
%     'MIPGap',2e-2);
% params_controller=generate_params_MPC(24 * 365,[1,3,6,24*[1,2,7,28]],[3,3,2,3,2,3,12],...
%     'B_reshuffle',1,[nan,nan,nan,8,8,8,8], ...
%     'terminalConstraint','none', ...
%     'terminalCost','linear',-4150*0.8, ...
%     'MIPGap',1e-4);
% params_controller=generate_params_MPC(24 * 365 *2,[1,3,6,24*[1,2,7,14,40,5,40]],[3,3,2,1,2,1,8,6,1,9],...
%     'B_reshuffle',1,[nan,nan,nan,8,8,8,8,8,nan,nan], ...
%     'terminalConstraint','lastYear',50, ...
%     'MIPGap',5e-2);
params_controller=generate_params_MPC(24 * 365*2,24*repmat([5,30],1,2),repmat([1,12],1,2),...
    'terminalConstraint','lastYear',20);
% get all paramters which are constant in time
params_constant=generate_params_constant;
% get the predicted external factors (from data)
prediction=generate_prediction('');

% generate MPC class object and then solve for a specified initial
% condition
MPC_obj = MPC(params_controller,params_constant);
T_BTES_0=20;

[U, X, ctrl_info,log_obj]=MPC_obj.eval(T_BTES_0,prediction);
params_controller=MPC_obj.params_MPC;

%[X_check, ctrl_info_check]=MPC_obj.checkTraj(T_BTES_0,U,prediction);

data=struct;
data.out=struct;
data.out.X=X;
data.out.U=U;
data.out.t_solve=ctrl_info.solvetime;
data.out.ctrl_info=ctrl_info;
data.params_constant=params_constant;
data.params_controller=params_controller;
data.controller_obj=MPC_obj;
data.prediction=prediction;

%%
cost_traj=get_cost_traj(data.out.U,data.out.X,data.prediction,data.params_constant,data.params_controller.N_j,data.params_controller.dt_j);
fprintf('Optimization objective: %0.2f kCHF, Traj cost: %0.2f kCHF, Diff: %0.2f \n',data.out.ctrl_info.objective/1000,cost_traj/1000,(data.out.ctrl_info.objective-cost_traj)/1000);

data.cost_traj=cost_traj;

%% plot
plot_yearly(data.out.X,data.out.U,log_obj,data.params_constant,data.params_controller);

%plot_energy(X,U,params_constant,params_offline,T_BTES_0,prediction);

%% save data

if ~isempty(testname)
    datename=char(datetime,'yyMMdd_HHmm');
    filename=[datename,'_','OL'];
    if ~isempty(testname)
        filename=[filename,'_',testname];
    end
    
    save(['../simulation_output/',filename,'.mat'],'-struct','data');
end