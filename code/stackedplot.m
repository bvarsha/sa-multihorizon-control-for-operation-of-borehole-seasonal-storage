%load data
type='SS'; %SS, MPC
num=2;
switch type
    case 'SS'
        % MHMPC MHMPC+
        % data1=load('../simulation_output/231214_1126_SS_MHMPC_sc20_1h_try3.mat');
        % data2=load('../simulation_output/231218_1359_SS_MHMPC_reshuffle_sc50.mat');
        % data3=load('../simulation_output/231223_1438_SS_MHMPCplus_terminalCost.mat');

        % % MPC highlevel and 24h
        % data1=load('../simulation_output/231219_2006_SS_MPC_highlevel_try.mat');
        % data2=load('../simulation_output/231203_1542_SS_MPC_24h.mat');
        
        % % RBC_RBC9_closedloop
        % data1=load('../simulation_output/231214_1317_SS_rulebased_7_1h.mat');
        % data2=load('../simulation_output/231226_1800_SS_RBC_9.mat');

        % % heat-33 cool50
        % data1=load('../simulation_output/240101_1229_SS_MHMPCplus_heat-33_cool50.mat');
        % data2=load('../simulation_output/231226_2136_SS_MPC_24h_heat-33_cool50.mat');
        % data3=load('../simulation_output/231226_2312_SS_RBC_7_heat-33_cool50.mat');

        % % heat-33 cool50 MHMPC+
        % data1=load('../simulation_output/240101_1229_SS_MHMPCplus_heat-33_cool50.mat');
        % data2=load("../simulation_output/240102_0231_SS_MHMPC+TC_heat-33_cool50.mat");

        % heat-33 cool50 MPC 24h RBC
        data1=load('../simulation_output/231226_2136_SS_MPC_24h_heat-33_cool50.mat');
        data2=load("../simulation_output/231226_2312_SS_RBC_7_heat-33_cool50.mat");

        params_controller_1=data1.params_controller;
        params_controller_2=data2.params_controller;

        params_sim_1=data1.params_sim;
        params_sim_2=data2.params_sim;

        U_1=data1.out.U_SS;
        X_1=data1.out.X_SS;
        U_2=data2.out.U_SS;
        X_2=data2.out.X_SS;

        if num==3
            params_controller_3=data3.params_controller;
            params_sim_3=data3.params_sim;
            U_3=data3.out.U_SS;
            X_3=data3.out.X_SS;
        end

    case 'MPC'
        T_BTES_0=30;

        params_controller_1=generate_params_MPC(24 * 365,24*2,365/2,'terminalConstraint','initialCond','verbose',2);
        params_controller_2=generate_params_MPC(24 * 365,24*[2,4,7,14,28],[6,6,3,4,9],'terminalConstraint','initialCond');
        params_constant=generate_params_constant;
        prediction=generate_prediction('');
        
        MPC_obj_1 = MPC(params_controller_1,params_constant);
        MPC_obj_2 = MPC(params_controller_2,params_constant);

        [U_1, X_1, ~, ~]=MPC_obj_1.eval(T_BTES_0,prediction);
        [U_2, X_2, ~, ~]=MPC_obj_2.eval(T_BTES_0,prediction);
end

%%
fig=figure;

pos_orig=fig.Position;
switch num
    case 2
        fig.Position=pos_orig.*[1,1,1.05,1.5];
    case 3
        fig.Position(3:4)=pos_orig(3:4).*[1.05,1.92];
end

set(fig, 'DefaultTextInterpreter', 'latex')
set(fig, 'DefaultLegendInterpreter', 'latex')
set(fig, 'DefaultAxesTickLabelInterpreter', 'latex')
set(fig, 'DefaultLineLineWidth', 1);

switch type
    case 'SS'
        switch num
            case 2
                subplot(2,1,1)
                plot_yearly(X_1,U_1,[],params_constant,params_controller_1,'ax',gca,'type','CL',params_sim_1,'B_resample',1);
        
                subplot(2,1,2)
                plot_yearly(X_2,U_2,[],params_constant,params_controller_2,'ax',gca,'type','CL',params_sim_2,'B_resample',1);
            case 3
                subplot(3,1,1)
                plot_yearly(X_1,U_1,[],params_constant,params_controller_1,'ax',gca,'type','CL',params_sim_1,'B_resample',1);
        
                subplot(3,1,2)
                plot_yearly(X_2,U_2,[],params_constant,params_controller_2,'ax',gca,'type','CL',params_sim_2,'B_resample',1);

                subplot(3,1,3)
                plot_yearly(X_3,U_3,[],params_constant,params_controller_3,'ax',gca,'type','CL',params_sim_3,'B_resample',1);
        end
    case 'MPC'
        subplot(2,1,1)
        plot_yearly(X_1,U_1,[],params_constant,params_controller_1,'ax',gca);

        subplot(2,1,2)
        plot_yearly(X_2,U_2,[],params_constant,params_controller_2,'ax',gca);
end