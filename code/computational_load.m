%% load data
data=cell(6,1);
labels={'MHMPC','MHMPC+','MHMPC+ (TC)','RBC','24h MPC','Highlevel MPC'}';
data{1}=load("../simulation_output/231214_1126_SS_MHMPC_sc20_1h_try3.mat");
data{2}=load("../simulation_output/231223_1425_CL_MHMPC_plus.mat");
data{3}=load("../simulation_output/231226_1515_SS_MHMPC_plus_terminalCost.mat");
data{4}=load("../simulation_output/231214_1317_SS_rulebased_7_1h.mat");
data{5}=load("../simulation_output/231203_1542_SS_MPC_24h.mat");
data{6}=load("../simulation_output/231219_2006_SS_MPC_highlevel_try.mat");

%%

t_solve=nan(length(data),8760);
for i_c=1:length(data)
    t_solve(i_c,:)=data{i_c}.out.t_solve(1:8760);
end

%% plot

fig = figure();
fig.Position(3)=1.05*fig.Position(3);
fig.Position(4)=400;
set(fig, 'DefaultTextInterpreter', 'latex')
set(fig, 'DefaultLegendInterpreter', 'latex')
set(fig, 'DefaultAxesTickLabelInterpreter', 'latex')
set(fig, 'DefaultLineLineWidth', 1);
sz_font=16;

semilogy(t_solve')
xlim([0,8760])
ylim([0,1e4])

xlabel('$k$')
ylabel('$t_{comp} [s]$')

legend(labels,'Location','north','NumColumns',3)


fontsize(fig, sz_font, "points")