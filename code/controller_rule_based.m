classdef controller_rule_based
    properties
        params_controller
        params_const
        params_constraints
        create_time
    end

    methods
        function obj = controller_rule_based(params_controller,params_const)
            tic

            obj.params_controller=params_controller;
            obj.params_const=params_const;

            obj.params_constraints=struct;
            obj.params_constraints.P_hp_BT_max=obj.params_const.UA_heat*obj.params_const.deltaT_hp_BT;
            obj.params_constraints.P_ch_BT_max=obj.params_const.UA_cool*obj.params_const.deltaT_ch_BT;

            obj.create_time=toc;
        end

        function [u, ctrl_info] = eval(obj,x,pred)
            tic

            avg_interval=1:size(pred,2);
            pred=average_prediction(pred,avg_interval);

            P_sol=obj.params_const.eta_sol*obj.params_const.A_sol*pred(1);
            T_a=pred(2);
            demand_heat=pred(4);
            demand_cool=pred(5);
            
            P_hp_BT=0;
            P_ch_BT=0;
            P_hp_a=0;
            P_ch_a=0;
            P_BT_direct=0;
            P_sol_used=0;
            P_sol_tr=0;

            if demand_heat>0
                if demand_heat<P_sol
                    P_sol_used=demand_heat;
                    P_sol_tr=P_sol-P_sol_used;
                else
                    P_sol_used=P_sol;
                    P_heat_remaining=demand_heat-P_sol;
                    if T_a<obj.params_controller.T_a_threshold
                        if P_heat_remaining<obj.params_constraints.P_hp_BT_max
                            P_hp_BT=P_heat_remaining;
                        else
                            P_hp_BT=obj.params_constraints.P_hp_BT_max;
                            P_hp_a=P_heat_remaining-P_hp_BT;
                        end
                    else
                        P_hp_a=P_heat_remaining;
                    end
                end
            elseif demand_cool>0
                P_sol_tr=P_sol;
                if x>=obj.params_const.T_BT_max
                    P_sol_tr=0;
                    P_sol_used=P_sol; %discard heat
                    P_ch_a=demand_cool;
                elseif x>=obj.params_const.T_BT_max-0.2
                    P_ch_a=demand_cool;
                else
                    if demand_cool<obj.params_constraints.P_ch_BT_max
                        P_ch_BT=demand_cool;
                    else
                        P_ch_BT=obj.params_constraints.P_ch_BT_max;
                        P_ch_a=demand_cool-P_ch_BT;
                    end
                end
            end
            
            u=[...
            P_hp_BT;...
            P_ch_BT;...
            P_hp_a;...
            P_ch_a;...
            P_BT_direct;...
            P_sol_used;...
            P_sol_tr;...
            ];

            solvetime=toc;
            ctrl_info = struct('solvetime',solvetime,'ctrl_feas',1);
        end
    end
    
end
