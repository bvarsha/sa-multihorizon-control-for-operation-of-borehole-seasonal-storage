function [x_next,P_BT_loss_top,P_BT_loss_g]=dynamics_1C(x,u,params_const,pred,dt)
    % dynamics
    P_BT_loss_top=params_const.U_i*params_const.D^2/4*pi()*(x-pred(2));
    P_BT_loss_g=params_const.k_g*params_const.h*params_const.D/2*(x-params_const.T_g);

    % dynamics parameters
    kappa=60*60/params_const.c_p_g_vol/params_const.V;
    UA_BT_top=params_const.U_i*params_const.D^2/4*pi();
    UA_BT_g=params_const.k_g*params_const.h*params_const.D/2;
    A = 1 - dt*kappa*(UA_BT_top+UA_BT_g);
    B_T = [-1 1 0 0 -1 0 1]'*dt*kappa; %B transpose
    C = dt*kappa.*(UA_BT_top*pred(2)+UA_BT_g*params_const.T_g);
    
    % next state
    x_next=A*x+B_T'*u+C;

end