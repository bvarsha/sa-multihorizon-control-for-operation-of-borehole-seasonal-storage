function [params] = generate_params_constant()

% model
params = struct(...
    'nx', 1, ...                    % -
    'nu', 7, ...                    % -
    'np', 5, ...                    % -
    'UA_heat', 1.61*50.6, ...       % kW/K
    'UA_cool', 50.6, ...            % kW/K
    'deltaT_ch_BT', 10, ...         % K
    'deltaT_hp_BT', 10, ...         % K
    'deltaT_ch_a', 10, ...          % K
    'deltaT_hp_a', 10, ...          % K
    'T_g', 12, ...                  % °C
    'k_g', 2.4*10^-3, ...           % kW/(m K)
    'h',  21.2, ...                 % -, heat loss coefficient
    'D', 70, ...                    % m
    'V', 269400, ...                % m^3
    'c_p_g_vol',2200,...            % kJ/(K*m^3)
    'T_d_hs', 65, ...               % °C
    'T_d_cs', 6, ...                % °C
    'U_i', 0.14*10^-3, ...          % kW∕m^2 K,conductivity top --> fitted
    'eta_sol', 0.65, ...            % -
    'A_sol', 2000, ...              % m^2  --> fitted
    'a_ch', 5.52*10^-3, ...         % -
    'b_ch', -5.14*10^-3, ...        % -
    'a_hp', -5.92*10^-3, ...        % -
    'b_hp', 0.3844, ...             % -
    'nu_CO2', 0.1, ...              % CHF/kgCO2
    'nu_el', 0.156, ...             % CHF/kWh
    'T_BT_max',65, ...              % °C
    'T_BT_min',6, ...               % °C
    'P_hp_a_max',10^4, ...          % kW
    'P_ch_a_max',10^4 ...           % kW
);

% option to set constant hp/chiller efficiency
% T_const=30;
% params.b_ch=params.a_ch*T_const+params.b_ch;
% params.a_ch=0;
% params.b_hp=params.a_hp*T_const+params.b_hp;
% params.a_hp=0;
end
