prediction=generate_prediction;

Fs = 24;                % Sampling frequency                    
T = 1/Fs;               % Sampling period

y = prediction(3,:);
L=length(y);            % Length of signal
t = (0:L-1)*T;          % Time vector

NFFT = 2^nextpow2(L);
Y = abs(fft(y,NFFT))/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
T_out=1./f;

figure;
semilogx(T_out,2*abs(Y(1:NFFT/2+1)))
xlim([0.1,10])

