function [params_off] = generate_params_offline(params_const,prediction)

T_a=prediction(2,:);

% computations

params_off.COP_hp_a=...
    0.5*(params_const.T_d_hs+273.15)./(params_const.T_d_hs-T_a+params_const.deltaT_hp_a);
% params_off.COP_ch_a=min(100,...
%     0.5*(params_const.T_d_cs+273.15)./(T_a+params_const.deltaT_ch_a-params_const.T_d_cs));
params_off.COP_ch_a=...
    0.5*(params_const.T_d_cs+273.15)./(T_a+params_const.deltaT_ch_a-params_const.T_d_cs);

end
