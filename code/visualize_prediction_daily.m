prediction=generate_prediction();

I_sol=reshape(prediction(1,:),[24,365])';
T_a=reshape(prediction(2,:),[24,365])';
I_CO2=reshape(prediction(3,:),[24,365])';
L_heat=reshape(prediction(4,:),[24,365])';
L_cool=reshape(prediction(5,:),[24,365])';

figure
xgrid=(1:365)'*ones(1,24);
ygrid=ones(365,1)*(1:24);

subplot(5,1,1);
pcolor(xgrid,ygrid,I_sol)
title('I_sol','Interpreter','none');

subplot(5,1,2);
pcolor(xgrid,ygrid,T_a)
title('T_a','Interpreter','none');

subplot(5,1,3);
pcolor(xgrid,ygrid,I_CO2)
title('I_CO2','Interpreter','none');

subplot(5,1,4);
pcolor(xgrid,ygrid,L_heat)
title('L_heat','Interpreter','none');

subplot(5,1,5);
pcolor(xgrid,ygrid,L_cool)
title('L_cool','Interpreter','none');