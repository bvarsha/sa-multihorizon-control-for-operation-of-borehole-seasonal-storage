prediction=generate_prediction;

data=load('InputData_BTES_Empa_CaseStudy.mat');

heatDemand=smoothdata(data.heatDemand','movmean',24);
coolDemand=smoothdata(data.coolDemand','movmean',24);
prediction=smoothdata(prediction,2,'movmean',24);


xRange=((0:length(prediction)-1)/24);

fig = figure(); 
fig.Position(4)=600;
set(fig, 'DefaultTextInterpreter', 'latex')
set(fig, 'DefaultLegendInterpreter', 'latex')
set(fig, 'DefaultAxesTickLabelInterpreter', 'latex')
set(fig, 'DefaultLineLineWidth', 1);
hold on
sz_font=16;
subplot(3,1,1);
ax1=gca;

plot(xRange,[heatDemand;prediction(4,:);-coolDemand;-prediction(5,:)]);
legend('$L_{heat,raw}$','$L_{heat}$','$L_{cool,raw}$','$L_{cool}$','Location','northwest','NumColumns',2);
colororder(ax1,[ ...
    0.4118    0.1569    0.0471; ...
    0.8500    0.3250    0.0980; ...
    0    0.1725    0.2902; ...
    0    0.4470    0.7410; ...
    ]);
xlabel('$t \ [d]$')
ylabel('$[kW]$')

subplot(3,1,2);
ax2=gca;
yyaxis left
plot(xRange,prediction(2,:))
ylabel('$[^\circ C]$')
ylim([round(0.1*min(prediction(2,:)))*10,round(0.1*max(prediction(2,:)))*10+20])
yyaxis right
plot(xRange,prediction(1,:))
ylabel('$[kW/m^2]$')
ylim([round(min(prediction(1,:)),1),round(max(prediction(1,:)),1)+0.2])
xlabel('$t \ [d]$')

ax2.YAxis(1).Color = 'k';
ax2.YAxis(2).Color = 'k';

legend({'$T_{a}$','$I_{sol}$'},'Location','northwest','NumColumns',2);

subplot(3,1,3)
ax3=gca;
plot(xRange,prediction(3,:))
legend({'$I_{C02}$'},'Location','northwest');
xlabel('$t \ [d]$')
ylabel('$[kgCO2/kWh]$')

linkaxes([ax1,ax2,ax3],'x');
xlim([0,max(xRange)]);

fontsize(fig, sz_font, "points")

% filename=''
% print2PDF(filename,fig)