function plot_yearly(X,U,log_obj,params_const,params_MPC,varargin)
type='MPC';
ax=[];
B_resample=0;
% varargin processing
i=1;
i_iter=1;
while i<=length(varargin)
    switch varargin{i}
        case 'type'
            type=varargin{i+1};
            switch type
                case 'MPC'
                    i=i+2;
                case 'CL'
                    params_sim=varargin{i+2};
                    i=i+3;
            end
        case 'ax'
            ax=varargin{i+1};
            i=i+2;
        case 'B_resample'
            B_resample=varargin{i+1};
            i=i+2;
    end
    i_iter=i_iter+1;
    if i_iter>length(varargin)
        error('Varagin processing failed');
        break
    end
end

switch type
    case 'MPC'
        U=[U,nan(height(U),1)];
        xRange=(0:params_MPC.Tf)./24;
        
        if size(X,2)~=length(xRange)
            [X_plot,U_plot,log_obj_plot]=resample_solution(X,U,log_obj,params_const,type,params_MPC);
        else
            X_plot=X;
            U_plot=U;
            log_obj_plot=log_obj;
        end
    case 'CL'
        if B_resample
            [X_plot,U_plot]=resample_solution(X,U,log_obj,params_const,type,params_sim);
            xRange=(0:length(X_plot)-1)./24;
        else
            X_plot=X;
            U_plot=[U,nan(height(U),1)];
            xRange=(0:params_sim.dt:params_sim.Tf)./24;
        end
end
if isempty(ax)
    fig = figure(); 
    set(fig, 'DefaultTextInterpreter', 'latex')
    set(fig, 'DefaultLegendInterpreter', 'latex')
    set(fig, 'DefaultAxesTickLabelInterpreter', 'latex')
    set(fig, 'DefaultLineLineWidth', 1);
    B_stackedplot=0;
    fig.Position(3:4)=fig.Position(3:4).*[1.05,0.8];
else
    fig=ax.Parent;
    set(fig, 'DefaultTextInterpreter', 'latex')
    set(fig, 'DefaultLegendInterpreter', 'latex')
    set(fig, 'DefaultAxesTickLabelInterpreter', 'latex')
    set(fig, 'DefaultLineLineWidth', 1);
    B_stackedplot=1;
end
sz_font=16;
yyaxis right
plot(xRange,X_plot','Color','black')
if B_stackedplot
    ylim([30,90])
else
    ylim([10,60])
end
ylabel('$[^\circ C]$')
hold on
yyaxis left
area(xRange,-U_plot([2,4,7],:)')	
area(xRange,U_plot([1,3,6,5],:)')
colororder([0.8500, 0.3250, 0.0980;
             0, 0.4470, 0.7410;
             0.9290, 0.6940, 0.1250;
             0.8500, 0.3250, 0.0980;
             0, 0.4470, 0.7410;
             0.9290, 0.6940, 0.1250;
             1, 0, 0]);
legendentries={'$P_{ch,BT}$','$P_{ch,a}$','$P_{sol,tr}$','$P_{hp,BT}$','$P_{hp,a}$','$P_{sol,used}$','$P_{BT,direct}$','$T_{BT}$'};
B_remove_direct_dis=1; %remove direct discharge legend if not used
if B_remove_direct_dis && sum(U(5,1:end-1))==0
    legendentries{7}='';
end
legend(legendentries,'Location','northwest','NumColumns',3)
ylabel('$[kW]$')
ylimit=ylim;
if B_stackedplot
    ylim([-2000 4000])
else
    ylim([ylimit(1) ylimit(2)*1.3])
end

xlim([0,max(xRange)]);

xlabel('$t \ [d]$')

ax = gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';

fontsize(fig, sz_font, "points")

end