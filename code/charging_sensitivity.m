yalmip('clear')
clear all

% get all paramters which are constant in time
params_constant=generate_params_constant;
prediction=generate_prediction;

params_controller=generate_params_MPC(24 * 365,24*5,73, ...
    'terminalConstraint','forced',20);

T_BTES_0=30;
DeltaT=4:-1:-4;
T_BTES_end=T_BTES_0+DeltaT;

N_Tend=length(T_BTES_end);

cost=nan(N_Tend,1);
fig = figure(); 
set(fig, 'DefaultTextInterpreter', 'latex')
set(fig, 'DefaultLegendInterpreter', 'latex')
set(fig, 'DefaultAxesTickLabelInterpreter', 'latex')
set(fig, 'DefaultLineLineWidth', 1);
fig.Position(3)=1.05*fig.Position(3);
fig.Position(4)=530;
sz_font=16;
subplot(2,1,1)
hold on
box on
ax_state=gca;
subplot(2,1,2)
ax_cost=gca;

for j=1:N_Tend
    params_controller.x_end=T_BTES_end(j);
    controller_obj = MPC(params_controller,params_constant);
    [U, X, ctrl_info,log_obj]=controller_obj.eval(T_BTES_0,prediction);
    if ctrl_info.objective==0
        obj=nan;
    else
        obj=ctrl_info.objective;
    end
    cost(j)=obj;
    plot(ax_cost,DeltaT,cost./1000);
    plot(ax_state,(0:73)*5,X);
    drawnow
end

%%

i_balanced=find(T_BTES_end==T_BTES_0);
plot(ax_cost,DeltaT,(cost-cost(i_balanced))./1000);

xlabel(ax_cost,'$\Delta T_{BT} \ [^\circ C]$')
ylabel(ax_cost,'$\Delta J \ [kCHF]$')
xlim(ax_cost,[min(DeltaT),max(DeltaT)]);
ylim_cost=ylim;
hold on
box on
plot(ax_cost,DeltaT,4.30*DeltaT)
ylim(ylim_cost);
legend(ax_cost,"$\Delta J$","$S_{BT,charge} \cdot \Delta T_{BT}$",'Location','north');

xlabel(ax_state,'$t \ [d]$')
ylabel(ax_state,'$T_{BT} \ [^\circ C]$')
xlim(ax_state,[0,365])
ylim(ax_state,[20,60])

legend(ax_state,strcat(repmat("$\Delta T_{BT}=",length(DeltaT),1),string(DeltaT'),repmat("$",length(DeltaT),1)),'NumColumns',3,'Location','north');

fontsize(fig, sz_font, "points")




