classdef MPC_highlevel
    properties
        MPC_obj
        dt
        dt_high
        idx_comp
        u_hold
    end

    methods
        function obj = MPC_highlevel(dt,params_MPC,params_const,varargin)
            obj.MPC_obj = MPC(params_MPC,params_const,varargin{:});
            obj.dt=dt;
            obj.dt_high=params_MPC.dt_j(1);
            obj.idx_comp=1;
        end

        function [obj, u, ctrl_info] = eval(obj,T_BTES_0,pred)
            if obj.idx_comp==1
                [u,~,ctrl_info]=obj.MPC_obj.eval(T_BTES_0,pred);
                obj.u_hold=u(:,1);
            end
            
            
            %% make sure the demands are met
            tic
            u=obj.u_hold;
            u_org=obj.u_hold;
            p=pred(:,1);
            
            if p(4)>0 %heating mode
                P_heat_hold=u_org(1)+u_org(3)+u_org(5)+u_org(6);
                u(3)=max(0,p(4)-P_heat_hold+u_org(3));
            else %cooling mode
                u(4)=max(0,p(5)-u_org(2));
            end
            solvetime=toc;

            if obj.idx_comp~=1
                ctrl_info = struct('ctrl_feas',1,'solvetime',solvetime);
            end

            obj.idx_comp=mod(obj.idx_comp+1,obj.dt_high/obj.dt);
        end
    end
end