function params = generate_params_MPC(Tf,dt_j,N_j,varargin)
terminalConstraint='initialCond';
terminalCost='none';
B_initial_cond=1;
verbose=1;
B_reshuffle=0;
l_end=0;
x_end=0;
lastYearScaling=1;
MIPGap=1e-3;
dt_h=nan;
dt_day=nan;

% varargin processing
i=1;
i_iter=1;
while i<=length(varargin)
    switch varargin{i}
        case 'terminalConstraint'
            terminalConstraint=varargin{i+1};
            switch terminalConstraint
                case 'forced'
                    x_end=varargin{i+2};
                    i=i+3;
                case 'lastYear'
                    lastYearScaling=varargin{i+2};
                    i=i+3;
                otherwise
                    i=i+2;
            end
        case 'terminalCost'
            terminalCost=varargin{i+1};
            if strcmp(terminalCost,'linear')
                l_end=varargin{i+2};
                i=i+3;
            else
                i=i+2;
            end
        case 'B_initial_cond'
            B_initial_cond=varargin{i+1};
            i=i+2;
        case 'verbose'
            verbose=varargin{i+1};
            i=i+2;
        case 'B_reshuffle'
            B_reshuffle=varargin{i+1};
            dt_h=varargin{i+2};
            i=i+3;
        case 'MIPGap'
            MIPGap=varargin{i+1};
            i=i+2;
    end
    i_iter=i_iter+1;
    if i_iter>length(varargin)
        error('MPC params could not be assigned');
        break
    end
end

N_j=floor(N_j);

% if the specified intervals do not match up with the total horizon length
% we need do add or remove some of the intervals and at the end potentially
% add one extra interval to make up for the remaining difference
diff=Tf-sum(dt_j.*N_j);
if diff~=0
    disp('The sampling times of the controller have to be adjusted');
end
while diff~=0
    if diff<0
        if N_j(end)>0
            N_j(end)=N_j(end)-1;
        else
            N_j=N_j(1:end-1);
            dt_j=dt_j(1:end-1);
        end
    elseif diff>0
        if diff>dt_j(end)
            N_j(end)=N_j(end)+1;
        else
            dt_j=[dt_j,diff];
            N_j=[N_j,1];
        end
    end
    diff=Tf-sum(dt_j.*N_j);
end

if B_reshuffle
    dt_day=dt_j/24;
    dt_j_new=dt_h.*dt_day;
    dt_j_new(isnan(dt_h))=dt_j(isnan(dt_h));
    N_j_new=24./dt_h.*N_j;
    N_j_new(isnan(dt_h))=N_j(isnan(dt_h));
    N_j=N_j_new;
    dt_j=dt_j_new;
end

params = struct(...                 
    'Tf', Tf, ...             
    'dt_j', dt_j, ...              
    'N_j', N_j, ...
    'terminalConstraint', terminalConstraint, ...
    'terminalCost', terminalCost, ...
    'B_initial_cond', B_initial_cond, ...
    'verbose', verbose, ...
    'B_reshuffle', B_reshuffle, ...
    'x_end',x_end, ...
    'l_end',l_end, ...
    'lastYearScaling',lastYearScaling, ...
    'MIPGap',MIPGap,...
    'dt_h',dt_h,...
    'dt_day',dt_day);
end