function [prediction] = generate_prediction(varargin)

if ~isempty(varargin)
    specialCase=varargin{1};
else
    specialCase='';
end

% prediction format: (N_states * N_horizon)
% State order = [I_k,T_a,I_CO2,P_load_heat,P_load_cool]

data=load('InputData_BTES_Empa_CaseStudy.mat');

prediction=zeros(5,8760);

netDemand=data.heatDemand-data.coolDemand;

prediction(1,:)=data.irradiation.*10^-3; % kW/m^2
prediction(2,:)=data.Taa; % °C
prediction(3,:)=data.CO2*10^-3; % 
prediction(4,:)=max(0,netDemand); % kW
prediction(5,:)=-min(0,netDemand); % kW

% prediction=smoothdata(prediction,2,"movmean",24);
switch specialCase
    case ''
    case 'cool20'
        prediction(5,:)=1.2*prediction(5,:);
    case 'cool50'
        prediction(5,:)=1.5*prediction(5,:);
    case 'heat-50'
        prediction(4,:)=0.5*prediction(4,:);
    case 'heat-33_cool50'
        prediction(4,:)=0.67*prediction(4,:);
        prediction(5,:)=1.5*prediction(5,:);
    otherwise
        error('The input to generate_prediction is not valid');
end

end
