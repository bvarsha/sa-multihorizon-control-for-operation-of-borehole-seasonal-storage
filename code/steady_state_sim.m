yalmip('clear')
clear all
%%
type='SS'; %SS or CL

testname=input('Please enter the test name (leave empty to not save): ',"s");
controllerTypes={'MHMPC','MPC_reshuffle','rule_based','MPC_highlevel','none','MPC_24h'};
controllerType=controllerTypes{2}; %

% generate parameters, predictions and constant constraints
params_constant=generate_params_constant;
prediction=generate_prediction('heat-33_cool50');

T_BTES_0=36;

B_plot=1;
dt_plot=1;

dt=1;
Tf_1y=24*365;
N_1y=Tf_1y/dt;
maxYears=5; %for CL, this is the simulation horizon in years

prediction=prediction(:,1:Tf_1y);

tol=0.1; %tolerance to determine if convergence is reached [°C]

params_sim = struct('Tf',Tf_1y,'dt',dt,'N',N_1y,'type',type,'maxYears',maxYears);

nu = params_constant.nu;
nx = params_constant.nx;

U = nan(nu,N_1y);
X = nan(nx,N_1y+1);
t_solve = nan(1,N_1y);
B_feasible = nan(1,N_1y);

X(:,1)=T_BTES_0;
B_isMPC=0;
%% create the controller
switch controllerType
    case 'MHMPC'
        % params_controller=generate_params_MPC(24 * 365*2,24*5,73*2,...
        %     'terminalConstraint','lastYear',20, ...
        %     'MIPGap',5e-2);
        % params_controller=generate_params_MPC(24 * 365*2,24*[5,15,30,5,30],[4,3,10,1,12],...
        %     'terminalConstraint','lastYear',20, ...
        %     'MIPGap',5e-2);
        % params_controller=generate_params_MPC(24 * 365*2,[1,3,8,24,24*7,24*7*4,24,24*7*4],[6,6,6,5,7,11,1,13],...
        %     'terminalConstraint','lastYear',20, ...
        %     'MIPGap',5e-2);
        params_controller=generate_params_MPC(24 * 365*2,24*[1,3,7,28,1,28],[3,4,6,11,1,13],...
            'terminalConstraint','lastYear',20, ...
             'MIPGap',5e-2);
        controller_obj = MPC(params_controller,params_constant);
        B_isMPC=1;
    case 'MPC_reshuffle'
        % params_controller=generate_params_MPC(24 * 365 *2,[1,3,6,24*[1,2,7,28,1,28]],[3,3,2,3,2,3,12,1,13],...
        %     'B_reshuffle',1,[nan,nan,nan,8,8,8,8,nan,nan], ...
        %     'terminalConstraint','lastYear',50, ...
        %     'MIPGap',2e-2);
        params_controller=generate_params_MPC(24 * 365,[1,3,6,24*[1,2,7,28]],[3,3,2,3,2,3,12],...
            'B_reshuffle',1,[nan,nan,nan,8,8,8,8], ...
            'terminalConstraint','none', ...
            'terminalCost','linear',-4150, ...
            'MIPGap',1e-4);
        controller_obj=MPC(params_controller,params_constant);
        B_isMPC=1;
    case 'rule_based'
        params_controller=struct('T_a_threshold',7);
        controller_obj = controller_rule_based(params_controller,params_constant);
    case 'MPC_highlevel'
        params_controller=generate_params_MPC(24 * 365*2,24*5,365/5*2, ...
            'terminalConstraint','lastYear',20, ...
            'MIPGap',5e-2);
        % params_controller=generate_params_MPC(24 * 365*2,24*[5,10,15,30,5,30],[4,3,3,9,1,12], ...
        %     'terminalConstraint','lastYear', 20, ...
        %     'MIPGap',5e-2);
        controller_obj = MPC_highlevel(dt,params_controller,params_constant);
        B_isMPC=0;
    case 'MPC_24h'
        params_controller=generate_params_MPC(24,1,24,'terminalCost','linear',-4150,'terminalConstraint','none');
        controller_obj = MPC(params_controller,params_constant);
        B_isMPC=1;
    case 'none'
        params_controller=struct;
end

%%

t_zero=string(duration('0:0:0'));
f = waitbar(0,sprintf('Time running: %s \n Year %d: %d/%d',t_zero,1,0,Tf_1y),'Name','Steady state simulation');

t_start=datetime;
delta_X_0_y=nan(1,maxYears);

figure;
ax=gca;
i_plot=dt_plot;

if B_isMPC
    X_pred=nan(N_1y,sum(params_controller.N_j)+1);
    cost_pred=nan(1,N_1y);
end

for i_y=1:maxYears
    off_y=(i_y-1)*N_1y;

    X_0_y=X(:,1+off_y);

    % for MPC classes always save the predictions
    
    prediction_dt=nan(5,N_1y);

    for i=1:N_1y
        i_t=dt*(i-1)+1;
        prediction_shifted=[prediction(:,i_t:end),prediction(:,1:i_t-1)];
        prediction_dt(:,i)=mean(prediction_shifted(:,1:dt),2);
        switch controllerType
            case 'MHMPC'
                [u,x,ctrl_info]=controller_obj.eval(X(:,i+off_y),prediction_shifted);
                u=u(:,1);
            case 'MPC_reshuffle'
                [u,x,ctrl_info]=controller_obj.eval(X(:,i+off_y),prediction_shifted);
                u=u(:,1);
            case 'rule_based'
                [u,ctrl_info]=controller_obj.eval(X(:,i+off_y),prediction_shifted(:,1:dt));
            case 'none'
                u=zeros(params_constant.nu,1);
                ctrl_info=struct('solvetime',0,'ctrl_feas',1);
            case 'MPC_highlevel'
                [controller_obj,u,ctrl_info]=controller_obj.eval(X(:,i+off_y),prediction_shifted);
            case 'MPC_24h'
                [u,x,ctrl_info]=controller_obj.eval(X(:,i+off_y),prediction_shifted);
                u=u(:,1);
        end
        if B_isMPC
            X_pred(i+off_y,:)=x;
            cost_pred(i)=ctrl_info.cost_economic;
        end
        U(:,i+off_y)=u;
        t_solve(i+off_y)=ctrl_info.solvetime;
        B_feasible(i+off_y)=ctrl_info.ctrl_feas;
        [X(:,i+off_y+1),~,~]=dynamics_1C(X(:,i+off_y),U(:,i+off_y),params_constant,prediction_shifted,dt);
        
        if B_plot
            if i_plot>=dt_plot
                if B_isMPC
                    yyaxis left
                    plot(ax,(0:N_1y-1)*dt/24,reshape(X(1:end-1)',N_1y,[])');
                    yyaxis right
                    plot(ax,(0:N_1y-1)*dt/24,reshape(cost_pred'/2,N_1y,[])');
                else
                    plot(ax,(0:N_1y-1)*dt/24,reshape(X(1:end-1)',N_1y,[])');
                end
                drawnow;
                i_plot=0;
            end
            i_plot=i_plot+1;
        end

        t_run=datetime-t_start;
        waitbar(i/N_1y,f,sprintf('Time running: %s \n Year %d: %d/%d',t_run,i_y,i,N_1y));
    end
    
    % check convergence
    if strcmp(type,'SS')
        delta_X_0_y(i_y)=X(:,i+off_y+1)-X_0_y;
        cost=get_cost_traj(U(:,1+off_y:i+off_y),X(:,1+off_y:i+off_y+1),prediction,params_constant,[],dt);
        fprintf('The difference to the last inital temperature was %d. Cost: %d CHF \n',delta_X_0_y(i_y),cost);
    
        if abs(delta_X_0_y(i_y))<=tol
            X_SS=X(:,off_y+1:end);
            U_SS=U(:,off_y+1:end);
            fprintf('The simulation converged after %d years \n',i_y)
            break
        end

        if i_y==maxYears
            X_SS=X(:,off_y+1:end);
            U_SS=U(:,off_y+1:end);
            fprintf('The simulation stopped after %d years \n',i_y)
            break
        end 
    elseif i_y==maxYears
        break
    end

    X=[X,nan(nx,N_1y)];
    U=[U,nan(nu,N_1y)];

    if B_isMPC
        X_pred=[X_pred;nan(N_1y,sum(params_controller.N_j)+1)];
        cost_pred=[cost_pred,nan(1,N_1y)];
    end
end

close(f);

if any(~B_feasible)
    fprintf('The controller was not feasible in %d timesteps \n',sum(~B_feasible))
end

data=struct;
data.out=struct;
data.out.X=X;
data.out.U=U;
data.out.t_solve=t_solve;
data.params_constant=params_constant;
data.params_controller=params_controller;
data.params_sim=params_sim;
data.controller_obj=controller_obj;
data.prediction=prediction;

if strcmp(type,'SS')
    data.out.X_SS=X_SS;
    data.out.U_SS=U_SS;
end

%% evaluate cost
if ~isfield(data,'prediction')
    data.prediction=prediction;
end
terminalCost=4150;
switch type
    case 'SS'
        [cost,cost_cum] = get_cost_traj(data.out.U_SS,data.out.X_SS,data.prediction,data.params_constant,[],data.params_sim.dt);
        cost_yearly_uncorrected=cost*8760/data.params_sim.Tf;
        cost_yearly=cost_yearly_uncorrected+terminalCost*(data.out.X_SS(1)-data.out.X_SS(end));
        fprintf('Yearly cost (raw): kCHF %0.2f, corrected: kCHF %0.2f \n',cost_yearly_uncorrected/1000,cost_yearly/1000);
    case 'CL'
        prediction=repmat(data.prediction,1,maxYears);
        [cost,cost_cum] = get_cost_traj(data.out.U,data.out.X,prediction,data.params_constant,[],data.params_sim.dt);
end

data.out.cost=cost;
data.out.cost_cum=cost_cum;

%% save the output
if strcmp(type,'SS')
    data.out.cost_yearly=cost_yearly;
end

if ~isempty(testname)
    datename=char(datetime,'yyMMdd_HHmm');
    filename=[datename,'_',type];
    if ~isempty(testname)
        filename=[filename,'_',testname];
    end
    
    save(['../simulation_output/',filename,'.mat'],'-struct','data');
end

%% plot the results
switch type
    case 'SS'
        plot_yearly(data.out.X_SS,data.out.U_SS,[],data.params_constant,data.params_controller,'type','CL',data.params_sim,'B_resample',1);
    case 'CL'
        plot_yearly(data.out.X,data.out.U,[],data.params_constant,data.params_controller,'type','CL',data.params_sim,'B_resample',1);
        cost_cum=[0,data.out.cost_cum];
        cost_yearly=diff(cost_cum(8760*(0:maxYears)+1));
        temp_0_yearly=data.out.X(8760*(0:maxYears)+1);
        figure();
        yyaxis left
        plot(cost_yearly)
        yyaxis right
        plot(temp_0_yearly)
        %plot((0:params_sim.dt:length(data.out.cost_cum)-1)./24,data.out.cost_cum)
end